set nocompatible

" ====================================================================================================
" @@ VUNDLE, BUNDLE MANAGEMENT = 'vundle' 'bundle'
" ====================================================================================================

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
Bundle 'gmarik/vundle'

"Bundle 'Valloric/YouCompleteMe'
"Bundle 'SirVer/ultisnips'
Bundle 'Shougo/neocomplete'
Bundle 'Shougo/neosnippet'
Bundle 'Shougo/neosnippet-snippets'
"Bundle 'Shougo/vimproc'
Bundle 'kien/ctrlp.vim'
"Bundle 'jcf/vim-latex'
Bundle 'scrooloose/syntastic'
Bundle 'kchmck/vim-coffee-script'
Bundle 'Tabular'
Bundle 'mattn/emmet-vim'
"clojure Bundle 'tpope/vim-salve.git'
"clojure Bundle 'tpope/vim-projectionist.git'
"clojure Bundle 'tpope/vim-dispatch.git'
"clojure Bundle 'tpope/vim-fireplace.git'
"Bundle 'Quramy/tsuquyomi'
Bundle 'leafgarland/typescript-vim'

Bundle 'taglist.vim'
"Bundle 'AutoClose'
Bundle 'CSApprox'
Bundle 'ScrollColors'
Bundle 'LargeFile'
Bundle 'php.vim-html-enhanced'
Bundle 'html-improved-indentation'
"Bundle 'hexman.vim'
Bundle 'rails.vim'
"Bundle 'delimitMate.vim'
Bundle 'greplace.vim'
"Bundle 'breakindent_beta.vim'
"Bundle 'textutil.vim'
Bundle 'tpope/vim-fugitive.git'
Bundle 'rust-lang/rust.vim'
Bundle 'slim-template/vim-slim.git'
Bundle 'jade.vim'
Bundle 'pangloss/vim-javascript'
Bundle 'diffchar.vim'
Bundle 'purescript-contrib/purescript-vim.git'

filetype plugin indent on       " REQUIRED!

" Brief help
" :BundleList         - list configured bundles
" :BundleInstall(!)   - install(update) bundles
" :BundleSeach(!) foo - search(or refresh cache first) for foo
" :BundleClean(!)     - confirm(or auto-approve) removal of unused bundles
"
" to install vundle: 
" git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle command are not allowed.
" ====================================================================================================
" @@ USER-DEFINED FUNCTIONS 'functions' 'func' 'def'
" ====================================================================================================

function! s:dark_colorscheme()
    if (&t_Co >= 256)
        " colorscheme wombat256mod
        " colorscheme molokai
        colorscheme darkbone
        " colorscheme solarized
    else
        colorscheme wombatmod
    endif
endfunction

function! s:light_colorscheme()
    " colorscheme anotherdark
    colorscheme summerfruit256
    " colorscheme autumn
    " colorscheme bclear
endfunction

" :Diff will enter diff mode between current buffer and file
function! s:DiffWithSaved()
    let filetype=&ft
    diffthis
    vnew | r # | normal! 1Gdd
    diffthis
    exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! Diff call s:DiffWithSaved()

function! s:BuildAndRunCProgram()
    exe '!gcc % -o testprogram && ./testprogram'
endfunction
com! TestC call s:BuildAndRunCProgram()

" ====================================================================================================
" @@ WORKSPACE SETUP = 'workspace' 'ws'
" ====================================================================================================

if ($COLORTERM == 'gnome-terminal')
    set t_Co=256
endif

if (strftime("%H") <= 17 && strftime("%H") >= 6)
    call s:light_colorscheme()
else
    call s:dark_colorscheme()
endif

set backspace=indent,eol,start
set completeopt-=preview
set expandtab
set foldlevelstart=99
"set foldmethod=syntax
set history=50
set hlsearch
set incsearch
set laststatus=2
set mouse=a
set nobackup
set nolist
set nowrap
set number
set ruler
set scrolloff=4
set shiftwidth=2
set softtabstop=2
set showcmd
set switchbuf=usetab
set textwidth=0
set visualbell t_vb=
set wildmenu

if filereadable("/usr/local/lib/python2.7/site-packages/powerline/bindings/vim/plugin/powerline.vim")
  source /usr/local/lib/python2.7/site-packages/powerline/bindings/vim/plugin/powerline.vim
endif

set laststatus=2

syntax on

syn sync fromstart

" ====================================================================================================
" @@ COMMANDS = 'commands' 'cmd'
" ====================================================================================================

if (!exists(":Dark"))
    command Dark call s:dark_colorscheme()
    command Light call s:light_colorscheme()
endif

" ====================================================================================================
" @@ NEOCOMPLETE = 'neocomplete' 'autocomplete' 'acp'
" ====================================================================================================

let g:neocomplete#force_overwrite_completefunc = 1

" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'

" Define dictionary.
let g:neocomplete#sources#dictionary#dictionaries = {
    \ 'default' : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist',
    \ 'scheme' : $HOME.'/.gosh_completions'
        \ }

" Define keyword.
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

" AutoComplPop like behavior.
"let g:neocomplete#enable_auto_select = 1

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif
"let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
"let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
"let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'

" For perlomni.vim setting.
" https://github.com/c9s/perlomni.vim
let g:neocomplete#sources#omni#input_patterns.perl = '\h\w*->\h\w*\|\h\w*::'

" ====================================================================================================
" @@ NEOSNIPPET (REQUIRES NEOCOMPLCACHE) = 'neosnippet' 'snippets' 'snip'
" ====================================================================================================

" Recommended mapping suggested by Shougo, but seems to cause undesired behavior.
"imap <expr><TAB> neosnippet#expandable_or_jumpable() ? "\<Plug>(neosnippet_expand_or_jump)" : pumvisible() ? "\<C-n>" : "\<TAB>"

function! s:snippet_behavior()
    if neosnippet#expandable()
        return "\<Plug>(neosnippet_expand)"
    elseif neosnippet#jumpable()
        return pumvisible() ? "\<C-n>" : "\<Plug>(neosnippet_jump)"
    else
        return pumvisible() ? "\<C-n>" : "\<TAB>"
    endif
endfunction

" For snippet_complete marker.
if has('conceal')
  set conceallevel=2 concealcursor=i
endif

" Include custom snippets directory.
let g:neosnippet#snippets_directory='~/.vim/snippets/'

" ====================================================================================================
" @@ CTRLP = 'ctrlp' 'goto'
" ====================================================================================================

let g:ctrlp_map = '<C-P>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_custom_ignore = '\v\/public\/assets\/'
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files . --cached --exclude-standard --others']
let g:ctrlp_user_command = ['.hg', 'hg --cwd %s status -numac -I . $(hg root)']
let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:20,results:20'

set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.class

" Swap open in current window and open in new tab
"let g:ctrlp_prompt_mappings = {
"    \ 'AcceptSelection("e")': ['<c-t>'],
"    \ 'AcceptSelection("t")': ['<cr>', '<2-LeftMouse>'],
"    \ }

" ====================================================================================================
" @@ SYNTASTIC = 'syntastic' 'syntax checker'
" ====================================================================================================

let g:syntastic_python_python_exec = 'python3'
let g:syntastic_python_flake8_args = '--exclude=__* --max-line-length=200'
let g:syntastic_mode_map = {
  \ "mode": "active",
  \ "active_filetypes": [],
  \ "passive_filetypes": ["html"] }
let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = ' -std=c++11 -stdlib=libc++'

" ====================================================================================================
" @@ VIM-LATEX = 'vim-latex' 'latex'
" ====================================================================================================

set grepprg=grep\ -nH\ $*
let g:tex_flavor='latex'

" ====================================================================================================
" @@ MAPPINGS = 'mappings' 'map'
" ====================================================================================================

map <F8> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR>
map <silent> <C-N> :set invhlsearch<CR>

nmap Q gq
nmap <C-TAB> gt
nnoremap ;; :%s::cg<Left><Left><Left>
nnoremap ;' :%s::g<Left><Left><Left>
nnoremap ;/ :Gsearch -r  app<Left><Left><Left><Left>
nnoremap <C-l>c :TestC<CR>
nnoremap <C-l>s :SyntasticToggleMode<CR>
nnoremap <C-j> 10j
nnoremap <C-k> 10k
nnoremap X :q<CR>

inoremap jj <ESC>
inoremap {<CR> {<CR>}<ESC>O
inoremap ({<CR> ({<CR>});<ESC>O

imap <C-k> <Plug>(neosnippet_expand_or_jump)
smap <C-k> <Plug>(neosnippet_expand_or_jump)
xmap <C-k> <Plug>(neosnippet_expand_target)
imap <expr><TAB> <SID>snippet_behavior()
smap <expr><TAB> <SID>snippet_behavior()

noremap <F4> :TlistToggle<CR>

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return pumvisible() ? neocomplete#close_popup() : "\<CR>"
endfunction
inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><C-y>  neocomplete#close_popup()
inoremap <expr><C-e>  neocomplete#cancel_popup()
" Close popup by <Space>.
"inoremap <expr><Space> pumvisible() ? neocomplete#close_popup() : "\<Space>"

" Impact
nnoremap <C-l>m :rightbelow vs <C-r>%c<CR>

" ====================================================================================================
" @@ ABBREVIATIONS = 'abbreviations' 'abbrev'
" ====================================================================================================

abbreviate JW Joshua Wu
abbreviate JTW Joshua Tsubaki Wu

" ====================================================================================================
" @@ MISCELLANEOUS = 'miscellaneous' 'misc'
" ====================================================================================================

" On opening a file, jump to the last known cursor position if valid.
autocmd BufReadPost *
\ if line("'\"") > 0 && line("'\"") <= line("$") |
\   exe "normal g`\"" |
\ endif

" On saving this .vimrc, source it
autocmd BufWritePost ~/.vim/vimrc source ~/.vimrc

" On saving .tex files, generate its .pdf
autocmd BufWritePost *.tex exe 'silent !pdflatex % > /dev/null &'

" On saving .mrn files, compile it to .mrnc
autocmd BufWritePost *.mrn if @% !~ '\.flattened\.mrn$' | exe 'silent !gsv -s --file % &'
