" Vim syntax file
" Language: Compiled Marain
" Maintainer: Peter de Blanc
" Latest Revision: 11 February 2017
" Version: 0.1

if exists("b:current_syntax")
  finish
endif

syn match mrncLiteralToken 'L' nextgroup=mrncLiteral
syn match mrncLiteral '[^\t]*' contained

syn match mrncWeightToken 'W' nextgroup=mrncWeight
syn match mrncWeight '[^\t]*' contained

syn match mrncSpellToken 'S' nextgroup=mrncSpell
syn match mrncSpell '[^\t]*' contained

syn match mrncRegularToken 'R' nextgroup=mrncRegular
syn match mrncRegular '[^\t]*' contained

syn match mrncLineTypeToken 'T' nextgroup=mrncLineType
syn match mrncLineType '[^\t]*' contained

syn match mrncKeymapToken 'K' nextgroup=mrncKeymap
syn match mrncKeymap '[^\t]*' contained

syn match mrncFormToken '(' nextgroup=mrncFormLength
syn match mrncFormLength '[^\t]*' contained

syn match mrncVariableToken 'V' nextgroup=mrncVariableName
syn match mrncVariableName '[^\t]*' contained

let b:current_syntax = "mrnc"

hi def link mrncWeightToken SpecialChar
hi def link mrncWeight Float

hi def link mrncSpellToken SpecialChar
hi def link mrncSpell Identifier

hi def link mrncRegularToken SpecialChar
hi def link mrncRegular Keyword

hi def link mrncLiteralToken SpecialChar
hi def link mrncLiteral String

hi def link mrncLineTypeToken SpecialChar
hi def link mrncLineType SpecialChar

hi def link mrncKeymapToken SpecialChar
hi def link mrncKeymap String

hi def link mrncFormToken SpecialChar
hi def link mrncFormLength Number

hi def link mrncVariableToken SpecialChar
hi def link mrncVariableName Identifier
